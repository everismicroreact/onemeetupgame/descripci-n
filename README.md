La siguiente imagen representa el flujo de integración entre el front, los microservicios, los repositorios y el bus de eventos. Todos los microservicios: Posición, Preguntas y Registro fueron construidos con Spring Boot usando el IDE de Spring Spring Tool Suite 3 - Version: 3.9.8.RELEASE.

Tenga en cuenta que para usar Kakfa requiere la instalación de zookeeper, nosotros recomendamos usar el servicio gratuito de kafka en la nube (https://www.cloudkarafka.com/) y ajustar los paramentros de conexión. Igualmente, para mongo puede usar el servicio gratuito de mongo en la nube (https://cloud.mongodb.com/user#/atlas/login) .

<img src="https://gitlab.com/everismicroreact/onemeetupgame/descripci-n/raw/master/imagen_1.jpg"/>

El siguiente diagrama ilustra el frameworks y dependencias básicas usadas durante el desarrollo de cada microservicio. Revise la documentación en cada proyecto para entender como ejecutarlo.

<img src="https://gitlab.com/everismicroreact/onemeetupgame/descripci-n/raw/9237884fb33421dedc9404cd8febc515332bc384/imagen2.png"/>

Una vez logre desplegar los microservicios, la siguiente documentación le servirá para entender las apis expuestas:

**Ejemplo de APIS expuestas**

-	POST http://localhost:8080/game/register
-	GET http://localhost:8080/game/register/status 
-	GET http://localhost:8082/game/challenge
-	POST http://localhost:8082/game/challenge
-	GET http://localhost:8081/game/position
-	GET http://localhost:8081/game/position/status

**Detalle**


**1. SERVICIO DE REGISTRO**


*  *Request para el registro de usuario.*

**BASE PATH**: 
/game </br>

**PATH**:
/register </br>

**METODO**
POST - (Consume/produces: Content-Type:application/json) </br>

**Retorno**
Retorna 201 cuando el usuario fue correctamente creado.  404 En 2 casos PlayerAlreadyExistsException (identificacion repetida), VehicleAlreadyInUseException (placa repetida) </br>

**BODY**: </br>
```
{
  "player": {
    "user":"ojato2",
    "identification":"80162565_17"
  },
  "vehicle":
  {
    "color":"555556",
    "number":"ivz7317"
  }
  
}
```

*Request de status. Este sirve para determinar si aun se permite seguir registrando usuarios.*

**PATH**: /register/status

**GET** (produce: text/event-stream)

Retorna un flujo de datos en formato booleano. **true** en caso que se permita registrar y **false** en caso que el registro deba bloquearse.

*Request cambiar status de registro a false.* 
No es necesario construir interfaz grafica pues lo invocamos desde un soap ui, postman o modificamos el valor en base de datos. En todo caso:

**PATH**: /register/status

**PUT** (Consume/produces: Content-Type:application/json)

**Retorna** 201 si la modificación se realizó. ESTE SERVICIO NO SE INVOCA DESDE LA APP, YO LO CONSUMO MANUALMENTE.



**2.  SERVICIO PREGUNTAS**

*Generar pregunta aleatoria.*

**PATH**: /challenge

**GET** - (Consume/produces: Content-Type:application/json)

Respuesta http: 200

**RESPONSE BODY**
```
{
"id": "5d64981a1c9d440000e093ee",
"level": 1,
"description": "¿Cuànto es 2 * 2?",
"options":[
{
"description": "4",
"letterOption": "a",
"correct": true
},
{
"description": "2",
"letterOption": "b",
"correct": false
}
]
}
```

*Validar si la respuesta dada por un jugador es correcta.* 
Se envia la pregunta, con la opcion seleccionada, el jugador que responde y el tiempo en milisegundos que le tomo dar la respuesta. 

**PATH**: /challenge

**POST** - (Consume/produces: Content-Type:application/json)

Respuesta http: 202

**BODY**
```
{
  "question": {
"id": "5d6496dd1c9d440000e093ec",
"options":[
{
"letterOption": "b"
}
]
},
"player": {
"identification":"80162565_15",
"user":"ojato2"
},
  "miliseconds":"2500"
}
```

**RESPONSE BODY**

Boolean. **True** si la respuesta es correcta, **false** en caso contrario.


**3.	SERVICIO POSICIONAMIENTO**

*Posición en cada momento de todos los jugadores.*

**PATH**: /position

**GET** (produce: text/event-stream)

La respuesta es un flujo de datos. El Json de Respuesta tiene la forma:


```
{
"points": "234",
"player":
{
"user": "ojato",
"identification": "80162565"
},
"vehicle":
{
"color": "343552",
"number": "IVZ732"
},
"position":
{
"track": "1",
"line": "1",
"step": "5"
}
}
```

*Permitir seguir enviando preguntas o cambiando posiciones.* 
La idea de este servicio es informar al front si se debe impedir que un jugador siga moviendo su vehículo o respondiendo preguntas. El juego Termina

**PATH**: /position/status

**GET** (produce: text/event-stream)

Retorna un flujo de datos en formato booleano. **True** en caso que el juego continúe y **false** en caso que el juego haya finalizado.
